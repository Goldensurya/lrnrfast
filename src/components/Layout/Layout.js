import React from 'react';
import Toolbar from '../Navigation/Toolbar/Toolbar';


const layout = ( props ) => (
    <div>
        <Toolbar/>
        <main className="Content"> 
            {props.children} 
        </main>
    </div>
);

export default layout;