import React from 'react';
import userIcon from '../../../assets/images/user.svg';
const header = (props) => {

        return (
            <nav className="navbar navbar-default">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <a className="navbar-brand hidden-xs" href="javascript:void(0)"><i className="fas fa-bars"></i></a>
                        <a className="navbar-brand xs-button hidden-md hidden-sm hidden-lg" href="javascript:void(0)"><i className="fas fa-bars"></i></a>
                    </div>
                    <ul className="nav navbar-nav pull-left">
                        <li>
                            <a href="javascript:void(0)" className="search-wrapper">
                                <input type="text" className="search-box"/>
                                <i className="fas fa-search"></i>
                            </a>
                        </li>
                    </ul>
                    <ul className="nav navbar-nav pull-right">
                        <li>
                            <a href="javascript:void(0)"><i className="fas fa-user-plus"></i>INVITE TEAM MEMBER</a>
                        </li>
                        <li><a href="javascript:void(0)"><i className="fas fa-bell"></i></a></li>
                        <li className="profile-image-wrapper dropdown">
                            <a href="javascript:void(0)" className="dropdown-toggle" id="menu1" data-toggle="dropdown">
                                <img src={userIcon} className="profile-image"/>
                            </a>
                            <ul className="dropdown-menu " role="menu" aria-labelledby="menu1">
                                <li role="presentation">
                                    <a role="menuitem" className="pull-left"  href="javascript:void(0)">Dark Mode</a>
                                   
                                    <div className="pull-right switch-btn">
                                        <label className="switch">
                                           
                                            <input
                                                id ="checkbox_id"
                                                type="checkbox"
                                                checked={props.changeToggle }
                                                onClick={props.handleCheck}
                                                />
                                            <span className="slider round"></span>
                                        </label>
                                    </div>    
                                </li>
                                <li role="presentation"><a role="menuitem" href="#">Profile</a></li>
                                <li role="presentation" className="divider"></li>
                                <li role="presentation"><a role="menuitem" href="#">What's new</a></li>
                                <li role="presentation"><a role="menuitem" href="#">Help</a></li>
                                <li role="presentation"><a role="menuitem" href="#">Send Feedback</a></li>
                                <li role="presentation"><a role="menuitem" href="#">Hints and shortcuts</a></li>
                                <li role="presentation" className="divider"></li>
                                <li role="presentation"><a role="menuitem" href="#">Log out</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        )
    }



    

export default header;