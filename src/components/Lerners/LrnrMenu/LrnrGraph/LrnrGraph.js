import React from 'react';


const lrnrGraph = (props) => (
    <div className="left-nav">
        <div className="row top-toolbar">
            <div className="col-sm-12">
                <div className="pull-left ptb-15">
                    <span>Graph</span>
                </div>
                <div className="pull-right">
                    <ul className="title-action-icons">
                        <li className="active">
                            <a href="#" ><i className="fas fa-plus"></i></a>
                        </li>
                        <li><a href="#"><i className="fas fa-expand"></i></a>
                        </li>
                        <li>
                            <a href="#"><i className="fas fa-angle-double-left"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div className="row plr-30">
            <div className="col-sm-12">
                <ul className="sideNav-menu-items-list-wrapper">
                    <li><i className="fas fa-angle-right"></i>Collection.1</li>
                    <li><i className="fas fa-angle-right"></i>Collection.2</li>
                    <li><i className="fas fa-angle-right"></i>Collection.3</li>
                    <li><i className="fas fa-angle-right"></i>Collection.4</li>
                    <li><i className="fas fa-angle-right"></i>Collection.5</li>
                </ul>
            </div>
        </div>
    </div>
    
);

export default lrnrGraph;