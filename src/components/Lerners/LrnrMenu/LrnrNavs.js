import React from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import LrnrAll from './LrnrAll/LrnrAll';
import LrnrBoard from './LrnrBoard/LrnrBoard';
import LrnrGraph from './LrnrGraph/LrnrGraph';
import LrnrRecent from './LrnrRecent/LrnrRecent';


const LrnrNavs = (props) => (
    <Router>
        <div className="left-nav2">
            <ul className="nav nav-tabs side-nav-tabs b-shadow" role="tablist">
                <li className="active">
                    <Link to="/">All</Link>
                </li>
                <li>
                    <Link to="/board">Board</Link>
                </li>
                <li>
                    <Link to="/graph">Graph</Link>
                </li>
                <li>
                    <Link to="/recent">Recent</Link>
                </li>
                <li>
                    <a href="#"><i className="fas fa-ellipsis-v"></i></a>
                </li>
            </ul>
        </div>
        <div>
            <Route exact path="/" component={LrnrAll} />
            <Route path="/board" component={LrnrBoard} />
            <Route path="/graph" component={LrnrGraph} />
            <Route path="/recent" component={LrnrRecent} />
        </div>
    </Router>
    
);

export default LrnrNavs;