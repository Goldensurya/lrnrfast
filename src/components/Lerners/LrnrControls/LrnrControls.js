
import React, { Component } from 'react';
import EventBus from 'vertx3-eventbus-client';

import AddMember from './AddMember/AddMember';
import WysiwgEditor from './WysiwgEditor/WysiwgEditor';
import Topic from './Topic/Topic';
import Widgets from './Widgets/Widgets';

class LrnrControls extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: ''
        }
    }


    componentDidMount() {
        this.eventBus = new EventBus('http://9cf7a64d.ngrok.io/eventbus');
        const eb = this.eventBus;
        eb.onopen = function (err, message) {
            if(err){
                console.log(' bus NOT conncected : ',err);
            }else{
        	    console.log(' bus conncected : ', eb.sockJSConn.url);
            }
        }
    }
    handleChangeText = (event, editor) => {
        const data = editor.getData();
        this.setState({data:data})
    }
 
    getAllData = (editor, evt) =>{
        const data = evt.getData();
        const eb = this.eventBus;
        const message = {
             message: data
        };
        eb.publish('editorTextIn', message);
        eb.registerHandler('editorTextOut', (err, message) => {
            if (!err) {
                console.log(' message : ',message);
            }
            else{
                console.log(' err : ',err);
            }
        })
    }


    render() {
        const datageta = this.state.data;

        //Bind the response data here which is comming from db
        const data = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release."

        let show;
        if(data){
            show =  <WysiwgEditor
                handleChangeText={this.handleChangeText}
                getAllData={this.getAllData}
                datageta={datageta}
                data={data}
            />
        }else{

            show = <WysiwgEditor
                handleChangeText={this.handleChangeText}
                getAllData={this.getAllData}
                datageta={datageta}
                data='There is no data inside editor please add'
            />

        }
        return (
            <div>
                <AddMember />
                {show}
                <Topic
                    handleChangeText={this.handleChangeText}
                    getAllData={this.getAllData}
                />
                <Widgets />
            </div>
        )
    }
}
export default LrnrControls;