import React, { Component } from 'react';
import LrnrControls from '../../components/Lerners/LrnrControls/LrnrControls';

class LrnrBuilder extends Component {
    
  render() {
    return (
        <div>
          <LrnrControls />
        </div>
    );
  }
}

export default LrnrBuilder;